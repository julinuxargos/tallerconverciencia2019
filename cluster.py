#cluster particional
import pandas as pd
from sklearn.cluster import KMeans

#datos
file='pobreza_municipios2011.csv'

#data frame
df = pd.read_csv(file)
#definir indices
df_index=df.set_index('Municipio')

#parametros de cluster
n=10
kmeans = KMeans(n_clusters=n)
#cluster
kmeans.fit(df_index)
#atributos
kmeans.cluster_centers_
kmeans.inertia_

#adjuntar clusters
df_clusters = pd.DataFrame(data = kmeans.labels_, columns = ['cluster'])
df_class = pd.concat([df, df_clusters], axis = 1)

#leer datos con cluster
cluster=0
df_class.loc[df_class['cluster'] == cluster]



#cluster jerarquico
import scipy.cluster.hierarchy as sch
#from sklearn.cluster import AgglomerativeClustering

#definir dendrograma
dendrogram = sch.dendrogram(sch.linkage(df_index, method='ward'),orientation='right',leaf_font_size=8, labels=df_index.index)
#mostrar dendrograma
plt.show()
#hc = AgglomerativeClustering(n_clusters=n, affinity = 'euclidean', linkage = 'ward')
#y_hc = hc.fit_predict(df_index)
